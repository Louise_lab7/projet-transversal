<?php
include_once 'connexion_1.php';

    session_start();
    $idrecup=$_SESSION['iduser'];

    $req2 = $bdd->prepare("SELECT * FROM documents_etudiant where id_document_etudiant = :id");
    $req2->execute(array(
            'id' => $idrecup
           ));

    $resultat2 = $req2->fetch();
    $type ='application/pdf';
    $filename = $resultat2['nom_document'];
    $file = $resultat2['fichier'];

    header("Content-type: $type");
    header("Content-Disposition: attachment; filename=$filename");
    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Expires: 0");
    header("Pragma: no-cache");
    readfile($file);
    echo $file;
    ob_clean();
    flush();
    exit;


?>